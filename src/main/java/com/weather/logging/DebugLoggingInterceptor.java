package com.weather.logging;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class DebugLoggingInterceptor {
	
	@Inject private LoggingService loggingService = LoggingService.getLoggingService();
	
	@AroundInvoke
	public Object debugLoggingInterceptor(InvocationContext ctx) throws Exception{
		System.out.println("Intercepting call to method: " + ctx.getMethod().getName());
		loggingService.debug("Intercepting call to method: " + ctx.getMethod().getName());
		return ctx.proceed();
	}
}
