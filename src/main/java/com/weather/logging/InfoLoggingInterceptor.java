package com.weather.logging;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class InfoLoggingInterceptor {
	
	@Inject private LoggingService loggingService = LoggingService.getLoggingService();

	@AroundInvoke
	public Object infoLoggingInterceptor(InvocationContext ctx) throws Exception{
		System.out.println("Intercepting call to method: " + ctx.getMethod().getName());
		loggingService.info("Intercepting call to method: " + ctx.getMethod().getName());
		return ctx.proceed();
	}
	
}
