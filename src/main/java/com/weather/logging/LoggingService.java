package com.weather.logging;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class LoggingService {
	private Log log = LogFactory.getLog(LoggingService.class);
	
	private static LoggingService loggingService = new LoggingService();
	
	//private constructor
	private LoggingService(){};
	
	public static LoggingService getLoggingService() {
		return loggingService;
	}
	
	public void info(String msg) {
		System.out.println(msg);
		log.info(msg);
	}
	
	public void warn(String msg) {
		log.warn(msg);
	}
	
	public void error(String msg) {
		log.error(msg);
	}
	
	public void debug(String msg) {
		log.debug(msg);
	}
}
