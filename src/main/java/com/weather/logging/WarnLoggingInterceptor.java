package com.weather.logging;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class WarnLoggingInterceptor {
	
	@Inject private LoggingService loggingService = LoggingService.getLoggingService();
	
	@AroundInvoke
	public Object warnLoggingInterceptor(InvocationContext ctx) throws Exception{
		System.out.println("Intercepting call to method: " + ctx.getMethod().getName());
		loggingService.warn("Intercepting call to method: " + ctx.getMethod().getName());
		return ctx.proceed();
	}

}
