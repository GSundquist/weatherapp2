package com.weather.dbutils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtil {
	
	public Connection con = getConnection();

	public Connection getConnection() {
		
		String connectionUrl = "jdbc:mysql://localhost:3306/cst361";
		Connection connection = null;
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
			connection = DriverManager.getConnection(connectionUrl, "admin", "password");
		}catch (InstantiationException e) {e.printStackTrace();}
		 catch (IllegalAccessException e) {e.printStackTrace();}
		 catch (ClassNotFoundException e) {e.printStackTrace();}
		 catch (SQLException e) {e.printStackTrace();}
		
		return connection;
	}
	
	public void closeConnection(Connection connection) {
		try {
			connection.close();
		}catch (SQLException e) {e.printStackTrace();}
	}
}
