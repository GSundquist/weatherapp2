package com.weather.user;

import javax.ejb.Remote;

@Remote
public interface UserDaoInterface {
	public User selectByUsernameAndPassword(String username, String password);
}
