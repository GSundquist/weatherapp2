package com.weather.user;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import com.weather.logging.*;
import com.weather.logging.LoggingService;
import com.weather.dbutils.DBUtil;

@Interceptors ({ErrorLoggingInterceptor.class})
@Stateless
public class UserDao implements UserDaoInterface{
	
	private DBUtil dbUtil = new DBUtil();
	@Inject LoggingService loggingService = LoggingService.getLoggingService();
	
	public User selectByUsernameAndPassword(String username, String password) {
		String sql = "select * from user where username=? and password=?";
		loggingService.info("Trying to process sql statement: " + sql);
		try {
			PreparedStatement statement = dbUtil.con.prepareStatement(sql);
			statement.setString(1, username);
			statement.setString(2, password);
			ResultSet results = statement.executeQuery();
			if(results.next()){
				int id = results.getInt("id");
				User user = new User(id, username, password);
				loggingService.info("End of function! Returning Id: " + user.getId());
				return user;
			}
			
		} catch (SQLException e) { 
			
			e.printStackTrace();
		} finally {
			dbUtil.closeConnection(dbUtil.con);
		}
		loggingService.info("End of function! Returning null");
		return null; 
	}

	public static void main(String[] args) {
		UserDao dao = new UserDao();
		User user = dao.selectByUsernameAndPassword("user", "user");
		System.out.println(user);
	}

}