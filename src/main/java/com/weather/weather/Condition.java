package com.weather.weather;

public class Condition {
	public float cloudCover;
	public float humidity; 
	public float pressure;
	public String weatherDesc;
	public String weatherIconUrl;
}
