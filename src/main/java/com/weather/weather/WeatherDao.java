package com.weather.weather;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.interceptor.Interceptors;

import com.weather.dbutils.DBUtil;
import com.weather.logging.*;

// DAO Design Pattern
public class WeatherDao {
	
	
	private DBUtil dbUtil = new DBUtil();
	// Singleton Design Pattern
	private static WeatherDao instance = null;
	protected WeatherDao() {}
	
	@Interceptors ({InfoLoggingInterceptor.class})
	public static WeatherDao getInstance() {
		if(instance == null)
			instance = new WeatherDao();
		return instance;
	}
	@Interceptors ({InfoLoggingInterceptor.class})
	private void create(Weather weather) {
		String sql = "insert into weather (zipcode, locationName) values (?,?)";
		try {
			PreparedStatement statement = dbUtil.con.prepareStatement(sql);
			statement.setString(1, weather.getZip());
			statement.setString(2, weather.getLocationname());
			statement.execute();
		} catch (SQLException e) {
		
			e.printStackTrace();
		} finally {
			dbUtil.closeConnection(dbUtil.con);
		}
	}
	@Interceptors ({InfoLoggingInterceptor.class})
	public List<Weather> selectFromUserId(int id) {
		List<Weather> ws = new ArrayList<Weather>();
		
		String sql = "select zipcode, locationName, weather.id from weather, user2weather, user where user.id = ? AND user.id = user2weather.userId AND weather.id = user2weather.id";
		
		try {
			PreparedStatement statement = dbUtil.con.prepareStatement(sql);
			statement.setInt(1, id);
			ResultSet results = statement.executeQuery();
			while(results.next()) {
				id = results.getInt("id");
				String zip = results.getString("zipcode");
				String locationName = results.getString("locationName");
				
				Weather weather = new Weather(id, locationName, zip);
				ws.add(weather);
			}
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		} 
		
		return ws;
	}
	
	@Interceptors ({DebugLoggingInterceptor.class})
	public static void main(String[] args) {
		// not mean't to be used many times, should be only one
		WeatherDao dao = WeatherDao.getInstance();
		
		Weather weather = new Weather("91950","California");
		
		dao.create(weather);
	}
	

}
